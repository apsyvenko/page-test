#!/bin/bash

SRC_CHANGED="$(git diff --name-only HEAD^..HEAD src)";
echo ${SRC_CHANGED}

if [[ $SRC_CHANGED ]]; then
    echo "Start";
    LOCAL_LIB=$(ls -1 src/lib-*.js);
    LIB_NAME=${LOCAL_LIB:4:3};
    LIB_VERSION=${LOCAL_LIB:8:5};
    LIB_NEW_VERSION="${LIB_VERSION%.*}.$((${LIB_VERSION##*.}+1))";
    LIB_LATEST="${LIB_NAME}-latest.js";

    mkdir -p "./dist"
    mkdir -p "./dist/${LIB_VERSION}";
    yes | cp ${LOCAL_LIB} "./dist/${LIB_VERSION}/${LIB_NAME}-${LIB_VERSION}.js";
    mkdir -p dist/latest;
    yes | cp ${LOCAL_LIB} "./dist/latest/${LIB_LATEST}";
    yes | cp src/index.html "./dist/index.html";

    NEW_LOCAL_LIB="./src/${LIB_NAME}-${LIB_NEW_VERSION}.js"
    mv ${LOCAL_LIB} ${NEW_LOCAL_LIB};

    git config user.email "siven5@live.com";
    git config user.name "Anatolii Syvenko";
    git remote set-url origin "https://apsyvenko:glpat-pksdE28P5mCyQySJHU1J@gitlab.com/apsyvenko/page-test.git"
    git checkout -B "${CI_COMMIT_REF_NAME}"
    git branch --set-upstream-to "origin/${CI_COMMIT_REF_NAME}" "${CI_COMMIT_REF_NAME}"
    git add "./${LOCAL_LIB}";
    git add "./${NEW_LOCAL_LIB}";
    git commit -m "New lib version ${LIB_VERSION} is released. [skip ci]";
    git push
fi
echo "Build is finished."